<?php
/**
 * Ease Car Listing.
 *
 * @package           iwpdev/ease-car-listing
 * @author            Alex Lavyhin
 * @license           GPL-2.0-or-later
 * @wordpress-plugin
 *
 * Plugin Name: Ease Car Listing.
 * Plugin URI: https://i-wp-dev.com
 * Description: Ease car listing for your site
 * Version: 1.0.0
 * Author: ALex Lavyhin
 * Author URI: https://i-wp-dev.com
 * License: GPL2
 *
 * Text Domain: ease-car-listing
 * Domain Path: /languages
 */

if ( ! defined( 'ABSPATH' ) ) {
	// @codeCoverageIgnoreStart
	exit;
	// @codeCoverageIgnoreEnd
}

use Iwpdev\EaseCarListing\Admin\Notification\Notification;
use Iwpdev\EaseCarListing\Main;


/**
 * Plugin version.
 */
const ECL_VERSION = '1.0.0';

/**
 * Plugin path.
 */
const ECL_PATH = __DIR__;

/**
 * Plugin main file
 */
const ECL_FILE = __FILE__;

/**
 * Min ver php.
 */
const ECL_PHP_REQUIRED_VERSION = '7.4';

/**
 * Plugin url.
 */
define( 'ECL_URL', untrailingslashit( plugin_dir_url( ECL_FILE ) ) );

/**
 * Access to the is_plugin_active function
 */
require_once ABSPATH . 'wp-admin/includes/plugin.php';

/**
 * Check php version.
 *
 * @return bool
 * @noinspection ConstantCanBeUsedInspection
 */
function is_php_version(): bool {
	if ( version_compare( constant( 'ECL_FILE' ), phpversion(), '>' ) ) {
		return false;
	}

	return true;
}

if ( ! is_php_version() ) {

	add_action( 'plugins_loaded', [ Notification::class, 'php_version_nope' ] );
	add_action( 'admin_notices', [ Notification::class, 'php_version_nope' ] );

	if ( is_plugin_active( plugin_basename( constant( 'ECL_FILE' ) ) ) ) {
		deactivate_plugins( plugin_basename( constant( 'ECL_FILE' ) ) );
		// phpcs:disable WordPress.Security.NonceVerification.Recommended
		if ( isset( $_GET['activate'] ) ) {
			unset( $_GET['activate'] );
		}
	}

	return;
}


require_once ECL_PATH . '/vendor/autoload.php';

new Main();
