<?php
/**
 * Archive actions and hooks.
 *
 * @package iwpdev/ease-car-listing
 */

namespace Iwpdev\EaseCarListing\Actions;

use Iwpdev\EaseCarListing\Helpers\LoadTemplate;

/**
 * Archive class file.
 */
class Archive {
	/**
	 * Archive construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init action and hooks.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'ecl_archive_filter', [ $this, 'before_archive_filter' ], 10 );
		add_action( 'ecl_archive_filter', [ $this, 'show_archive_filter' ], 20 );
		add_action( 'ecl_archive_filter', [ $this, 'after_archive_filter' ], 30 );
		add_action( 'ecl_archive_sort_filter', [ $this, 'before_archive_sort_filter' ], 10 );
		add_action( 'ecl_archive_sort_filter', [ $this, 'show_archive_sort_filter' ], 20 );
		add_action( 'ecl_archive_sort_filter', [ $this, 'after_archive_sort_filter' ], 30 );
		add_action( 'ecl_archive_filter_tag', [ $this, 'show_archive_filter_tag' ], 10 );
		add_action( 'ecl_archive_loop_listing', [ $this, 'before_archive_car_listing' ], 10 );
		add_action( 'ecl_archive_loop_listing', [ $this, 'show_archive_car_listing_item' ], 20 );
		add_action( 'ecl_archive_loop_listing', [ $this, 'after_archive_car_listing' ], 30 );
		add_action( 'ecl_before_archive_content', [ $this, 'start_wrap_archive_content' ], 10 );
		add_action( 'ecl_after_archive_content', [ $this, 'end_wrap_archive_content' ], 10 );
	}

	/**
	 * Get before archive filter.
	 *
	 * @return void
	 */
	public function before_archive_filter(): void {
		echo '';
	}

	/**
	 * Get after archive filter.
	 *
	 * @return void
	 */
	public function after_archive_filter(): void {
		echo '';
	}

	/**
	 * Get archive filter.
	 *
	 * @return void
	 */
	public function show_archive_filter(): void {
		LoadTemplate::ecl_get_template( '/template/archive/filter', 'filter' );
	}

	/**
	 * Start wrap archive content.
	 *
	 * @return void
	 */
	public function start_wrap_archive_content(): void {
		echo '<div class="cars-list">';
	}

	/**
	 * End wrap archive content.
	 *
	 * @return void
	 */
	public function end_wrap_archive_content(): void {
		echo '</div>';
	}

	/**
	 * Before archive sort filter
	 *
	 * @return void
	 */
	public function before_archive_sort_filter(): void {
		echo '<div class="dfr">';
	}

	/**
	 * After archive sort filter
	 *
	 * @return void
	 */
	public function after_archive_sort_filter(): void {
		echo '</div>';
	}

	/**
	 * Show filter archive.
	 *
	 * @return void
	 */
	public function show_archive_sort_filter(): void {
		LoadTemplate::ecl_get_template( '/template/archive/filter', 'sort' );
	}

	/**
	 * Show filter tag.
	 *
	 * @return void
	 */
	public function show_archive_filter_tag(): void {
		LoadTemplate::ecl_get_template( '/template/archive/filter', 'filter-tag' );
	}

	/**
	 * Before car listing
	 *
	 * @return void
	 */
	public function before_archive_car_listing(): void {
		echo '<div class="cars-items">';
	}

	/**
	 * After car listing
	 *
	 * @return void
	 */
	public function after_archive_car_listing(): void {
		echo '</div>';
	}

	/**
	 * Show car item.
	 *
	 * @return void
	 */
	public function show_archive_car_listing_item(): void {
		LoadTemplate::ecl_get_template( '/template/loop', 'item' );
	}
}