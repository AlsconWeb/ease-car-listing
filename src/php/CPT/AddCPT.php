<?php
/**
 * Create Custom post type.
 *
 * @package iwpdev/ease-car-listing
 */

namespace Iwpdev\EaseCarListing\CPT;

/**
 * AddCPT class file.
 */
class AddCPT {
	/**
	 * AddCPT construct
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'init', [ $this, 'register_post_auto' ] );
		add_action( 'init', [ $this, 'register_tax_mark' ] );
		add_action( 'init', [ $this, 'register_tax_engine' ] );


		add_filter( 'template_include', [ $this, 'add_single_car_page' ] );
		add_filter( 'template_include', [ $this, 'add_cars_archive' ] );
	}

	/**
	 * Register CPT Auto.
	 *
	 * @return void
	 */
	public function register_post_auto(): void {
		register_post_type(
			'cars',
			[
				'label'         => null,
				'labels'        => [
					'name'               => __( 'Cars', 'ease-car-listing' ),
					'singular_name'      => __( 'Car', 'ease-car-listing' ),
					'add_new'            => __( 'Add car', 'ease-car-listing' ),
					'add_new_item'       => __( 'Adding a car', 'ease-car-listing' ),
					'edit_item'          => __( 'Car editing', 'ease-car-listing' ),
					'new_item'           => __( 'New car', 'ease-car-listing' ),
					'view_item'          => __( 'View car', 'ease-car-listing' ),
					'search_items'       => __( 'Search car', 'ease-car-listing' ),
					'not_found'          => __( 'Not found', 'ease-car-listing' ),
					'not_found_in_trash' => __( 'Not found in cart', 'ease-car-listing' ),
					'menu_name'          => __( 'Cars', 'ease-car-listing' ),
				],
				'description'   => '',
				'public'        => true,
				'show_in_rest'  => null,
				'rest_base'     => null,
				'menu_position' => 30,
				'menu_icon'     => 'dashicons-car',
				'hierarchical'  => false,
				'supports'      => [ 'title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revisions' ],
				'taxonomies'    => [ 'cars' ],
				'has_archive'   => true,
				'rewrite'       => true,
				'query_var'     => true,
			]
		);
	}

	/**
	 * Register taxonomy mark & models.
	 *
	 * @return void
	 */
	public function register_tax_mark(): void {
		register_taxonomy(
			'mark',
			[ 'cars' ],
			[
				'label'             => '',
				'labels'            => [
					'name'              => __( 'Marks & Models', 'ease-car-listing' ),
					'singular_name'     => __( 'Marks & Models', 'ease-car-listing' ),
					'search_items'      => __( 'Search Mark', 'ease-car-listing' ),
					'all_items'         => __( 'All Marks & Models', 'ease-car-listing' ),
					'view_item '        => __( 'View Marks', 'ease-car-listing' ),
					'parent_item'       => __( 'Marks', 'ease-car-listing' ),
					'parent_item_colon' => __( 'Models', 'ease-car-listing' ),
					'edit_item'         => __( 'Edit Marks & Models', 'ease-car-listing' ),
					'update_item'       => __( 'Update Marks & Models', 'ease-car-listing' ),
					'add_new_item'      => __( 'Add New', 'ease-car-listing' ),
					'new_item_name'     => __( 'New Marks & Models', 'ease-car-listing' ),
					'menu_name'         => __( 'Marks & Models', 'ease-car-listing' ),
					'back_to_items'     => __( '← Back to Marks & Models', 'ease-car-listing' ),
				],
				'description'       => __( 'The main category is the brand of the car, the child category will be the model.', 'ease-car-listing' ),
				'public'            => true,
				'hierarchical'      => true,
				'rewrite'           => true,
				'capabilities'      => [],
				'show_admin_column' => true,
			] );
	}

	/**
	 * Register taxonomy engine.
	 *
	 * @return void
	 */
	public function register_tax_engine(): void {
		register_taxonomy(
			'engine',
			[ 'cars' ],
			[
				'label'             => '',
				'labels'            => [
					'name'              => __( 'Engine', 'ease-car-listing' ),
					'singular_name'     => __( 'Engine', 'ease-car-listing' ),
					'search_items'      => __( 'Search Engine', 'ease-car-listing' ),
					'all_items'         => __( 'All Engine', 'ease-car-listing' ),
					'view_item '        => __( 'View Engine', 'ease-car-listing' ),
					'parent_item'       => __( 'Parens Engine', 'ease-car-listing' ),
					'parent_item_colon' => __( 'Parent', 'ease-car-listing' ),
					'edit_item'         => __( 'Edit Engine', 'ease-car-listing' ),
					'update_item'       => __( 'Update Engine', 'ease-car-listing' ),
					'add_new_item'      => __( 'Add New', 'ease-car-listing' ),
					'new_item_name'     => __( 'New Engine', 'ease-car-listing' ),
					'menu_name'         => __( 'Engine', 'ease-car-listing' ),
					'back_to_items'     => __( '← Back to Engine', 'ease-car-listing' ),
				],
				'description'       => '',
				'public'            => true,
				'hierarchical'      => true,
				'rewrite'           => true,
				'capabilities'      => [],
				'show_admin_column' => true,
			] );
	}

	/**
	 * Add single car template.
	 *
	 * @param string $template
	 *
	 * @return string
	 */
	public function add_single_car_page( string $template ): string {

		if ( is_singular( 'cars' ) && ! locate_template( [ 'single-cars.php' ] ) ) {
			return ECL_PATH . '/template/single-cars.php';
		}

		return $template;
	}

	public function add_cars_archive( string $template ): string {
		if ( is_post_type_archive( 'cars' ) && ! locate_template( [ 'archive-cars.php' ] ) ) {
			return ECL_PATH . '/template/archive-cars.php';
		}

		return $template;
	}
}
