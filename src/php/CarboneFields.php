<?php
/**
 * Add Custom fields in project.
 *
 * @package iwpdev/ease-car-listing
 */

namespace Iwpdev\EaseCarListing;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CarboneFields class file.
 */
class CarboneFields {

	/**
	 * CarboneFields construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'after_setup_theme', [ $this, 'carbon_fields_init' ] );

		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbone_fields_to_cars' ] );
	}

	/**
	 * Carbone field init.
	 *
	 * @return void
	 */
	public function carbon_fields_init(): void {
		Carbon_Fields::boot();
	}

	/**
	 * Add Carbon fields to CPT cars
	 *
	 * @return void
	 */
	public function add_carbone_fields_to_cars(): void {
		Container::make( 'post_meta', __( 'Vehicle parameters', 'ease-car-listing' ) )
		         ->where( 'post_type', '=', 'cars' )
		         ->add_tab(
			         __( 'General', 'ease-car-listing' ),
			         [
				         Field::make( 'text', 'ecl_vin', __( 'Vin number', 'ease-car-listing' ) )->set_width( 50 ),
				         Field::make( 'text', 'ecl_stock_number', __( 'Stock number', 'ease-car-listing' ) )
				              ->set_width( 50 ),
				         Field::make( 'text', 'ecl_car_mileage', __( 'Сar mileage', 'ease-car-listing' ) )
				              ->set_width( 50 ),
				         Field::make( 'select', 'ecl_body_type', __( 'Body type', 'ease-car-listing' ) )
				              ->add_options(
					              [
						              'compact'      => __( 'Compact', 'ease-car-listing' ),
						              'sedan'        => __( 'Sedan', 'ease-car-listing' ),
						              'coupe'        => __( 'Coupe', 'ease-car-listing' ),
						              'hatchback'    => __( 'Hatchback', 'ease-car-listing' ),
						              'kombi'        => __( 'Kombi', 'ease-car-listing' ),
						              'limousine'    => __( 'Limousine', 'ease-car-listing' ),
						              'microvan'     => __( 'Microvan', 'ease-car-listing' ),
						              'minivan'      => __( 'Minivan', 'ease-car-listing' ),
						              'pickup'       => __( 'Pickup', 'ease-car-listing' ),
						              'roadster'     => __( 'Roadster', 'ease-car-listing' ),
						              'suv'          => __( 'SUV', 'ease-car-listing' ),
						              'wagon'        => __( 'Wagon', 'ease-car-listing' ),
						              'pickup_track' => __( 'Pickup track', 'ease-car-listing' ),
						              'van'          => __( 'Van', 'ease-car-listing' ),
					              ]
				              )->set_width( 50 ),
				         Field::make( 'select', 'ecl_fuel_type', __( 'Fuel type', 'ease-car-listing' ) )
				              ->add_options(
					              [
						              'gas'      => __( 'Gas', 'ease-car-listing' ),
						              'diesel'   => __( 'Diesel', 'ease-car-listing' ),
						              'electric' => __( 'Electric', 'ease-car-listing' ),
					              ]
				              )->set_width( 50 ),
				         Field::make( 'select', 'ecl_transmission_type', __( 'Transmission type', 'ease-car-listing' ) )
				              ->add_options(
					              [
						              'automatic' => __( 'Automatic', 'ease-car-listing' ),
						              'manual'    => __( 'Manual', 'ease-car-listing' ),
					              ]
				              )->set_width( 50 ),
				         Field::make( 'select', 'ecl_drive_train_type', __( 'Drive train type', 'ease-car-listing' ) )
				              ->add_options(
					              [
						              'fwd' => __( 'Front-wheel drive', 'ease-car-listing' ),
						              'rwd' => __( 'Rear-wheel drive', 'ease-car-listing' ),
						              '4wd' => __( 'Four-wheel drive', 'ease-car-listing' ),
						              'awd' => __( 'All-wheel drive', 'ease-car-listing' ),
					              ]
				              )->set_width( 50 ),
				         Field::make( 'text', 'ecl_car_year', __( 'Vehicle year', 'ease-car-listing' ) )
				              ->set_width( 50 ),
				         Field::make( 'text', 'ecl_horsepower', __( 'Vehicle Horsepower', 'ease-car-listing' ) )
				              ->set_width( 50 )
				              ->set_attribute( 'type', 'number' ),
				         Field::make( 'text', 'ecl_body_color', __( 'Body color', 'ease-car-listing' ) )
				              ->set_width( 50 ),
				         Field::make( 'text', 'ecl_interior_color', __( 'Interior color', 'ease-car-listing' ) )
				              ->set_width( 50 ),
				         Field::make( 'text', 'ecl_number_of_airbags', __( 'Number of airbags', 'ease-car-listing' ) )
				              ->set_width( 50 )
				              ->set_attribute( 'type', 'number' ),
				         Field::make( 'text', 'ecl_number_of_seats', __( 'Number of seats', 'ease-car-listing' ) )
				              ->set_width( 50 )
				              ->set_attribute( 'type', 'number' ),
				         Field::make( 'text', 'ecl_number_of_doors', __( 'Number of doors', 'ease-car-listing' ) )
				              ->set_width( 50 )
				              ->set_attribute( 'type', 'number' ),
			         ]
		         )
		         ->add_tab(
			         __( 'Additional Information', 'ease-car-listing' ),
			         [
				         Field::make( 'textarea', 'ecl_features', __( 'Features Information', 'ease-car-listing' ) )
				              ->set_help_text( __( 'Separate features with a comma and a space', 'ease-car-listing' ) ),
				         Field::make( 'rich_text', 'ecl_additional_info', __( 'Additional Information', 'ease-car-listing' ) ),
			         ]
		         )
		         ->add_tab(
			         __( 'Gallery', 'ease-car-listing' ),
			         [
				         Field::make( 'media_gallery', 'ecl_gallery', __( 'Gallery', 'ease-car-listing' ) )
				              ->set_type(
					              [
						              'image',
						              'video',
					              ]
				              ),
			         ]
		         )
		         ->add_tab(
			         __( 'Price', 'ease-car-listing' ),
			         [
				         Field::make( 'text', 'ecl_price', __( 'Price', 'ease-car-listing' ) )->set_width( 30 ),
				         Field::make( 'text', 'ecl_sales_price', __( 'Sales price', 'ease-car-listing' ) )
				              ->set_width( 30 ),
				         Field::make( 'text', 'ecl_by_month', __( 'Price per month', 'ease-car-listing' ) )
				              ->set_width( 30 ),
			         ]
		         );
	}
}
