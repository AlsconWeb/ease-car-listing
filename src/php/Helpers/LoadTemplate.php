<?php
/**
 * Load template hooks.
 *
 * @package iwpdev/ease-car-listing
 */

namespace Iwpdev\EaseCarListing\Helpers;

/**
 * LoadTemplate class file.
 */
class LoadTemplate {
	/**
	 * LoadTemplate construct
	 */
	public function __construct() {

	}

	/**
	 * Get templates.
	 *
	 * @param string $template_path Template path.
	 * @param string $template_name Template name.
	 *
	 * @return void
	 */
	public static function ecl_get_template( string $template_path, string $template_name ): void {
		$theme_location = get_stylesheet_directory() . $template_path . '/' . $template_name . '.php';

		if ( ! @file_exists( $theme_location ) ) {
			include ECL_PATH . $template_path . '/' . $template_name . '.php';

			return;
		}


		include $theme_location;
	}
}
