<?php
/**
 * Main cass file Ease car listing.
 *
 * @package iwpdev/ease-car-listing
 */

namespace Iwpdev\EaseCarListing;

use Iwpdev\EaseCarListing\Actions\Archive;
use Iwpdev\EaseCarListing\CPT\AddCPT;

/**
 * Main class file.
 */
class Main {

	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->init();

		new CarboneFields();
		new AddCPT();
	}

	/**
	 * Initials hooks.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );
		add_action( 'init', [ $this, 'custom_hooks' ] );
	}

	public function custom_hooks(): void {
		new Archive();
	}

	/**
	 * Add scripts and style.
	 *
	 * @return void
	 */
	public function add_scripts(): void {
		wp_enqueue_script( 'ecl_build', ECL_URL . '/assets/js/build.js', [ 'jquery' ], ECL_VERSION, true );
		wp_enqueue_script( 'html5shiv', '//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', [], '3.7.0', false );
		wp_enqueue_script( 'respond', '//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', [], '1.4.2', false );

		wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
		wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

		wp_enqueue_style( 'ecl_main', ECL_URL . '/assets/css/main.css', '', ECL_VERSION );

		$global_style = ':root{ --color-one:#f26b36; --color-two:#be542a;}';
		wp_add_inline_style( 'ecl_main', $global_style );
	}
}
