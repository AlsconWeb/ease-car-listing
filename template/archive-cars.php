<?php
/**
 * Archive cars template.
 *
 * @package iwpdev/ease-car-listing
 */

get_header();
?>
	<div class="container">
		<div class="dfr">
			<?php
			/**
			 * Hook: ecl_archive_filter.
			 *
			 * @hooked before_archive_filter - 10
			 * @hooked show_archive_filter - 20
			 * @hooked after_archive_filter - 30
			 */

			do_action( 'ecl_archive_filter' );

			/**
			 * Hook: ecl_before_archive_content.
			 *
			 * @hooked start_wrap_archive_content - 10
			 */
			do_action( 'ecl_before_archive_content' );

			/**
			 * Hook: ecl_archive_sort_filter.
			 *
			 * @hooked before_archive_sort_filter - 10
			 * @hooked show_archive_sort_filter - 20
			 * @hooked after_archive_sort_filter - 30
			 */
			do_action( 'ecl_archive_sort_filter' );

			/**
			 * Hook: ecl_archive_filter_tag
			 *
			 * @hooked show_archive_filter_tag - 10
			 */
			do_action( 'ecl_archive_filter_tag' );

			/**
			 * Hook: ecl_archive_loop_listing
			 *
			 * @hooked before_archive_car_listing - 10
			 * @hooked show_archive_car_listing_item - 20
			 * @hooked after_archive_car_listing - 30
			 */
			do_action( 'ecl_archive_loop_listing' );
			?>
		</div>
	</div>
<?php
/**
 * Hook: ecl_after_archive_content.
 *
 * @hooked end_wrap_archive_content - 10
 */
do_action( 'ecl_after_archive_content' );

get_footer();
