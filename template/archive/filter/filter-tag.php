<?php
/**
 * Tag form filter template.
 *
 * @package iwpdev/ease-car-listing
 */
?>
<ul class="filter-chosen dfr">
	<li>
		<span>Biweekly Payment: </span>
		$0 - $1,309
		<a class="remove-label" href="#">
			<i class="icon-clear"></i>
		</a>
	</li>
	<li>
		<span>Price: </span>
		$0 - $63,888
		<a class="remove-label" href="#">
			<i class="icon-clear"></i>
		</a>
	</li>
</ul>
