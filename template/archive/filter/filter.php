<?php
/**
 * Filter form template.
 *
 * @package iwpdev/ease-car-listing
 */
?>
<form class="filters left-sidebar">
	<div class="filters-result dfr">
		<p>12 filters</p><a class="clear-all">Clear All</a>
	</div>
	<hr class="sline">
	<div class="input drop">
		<h4 class="title icon-location">Location<i class="icon-plus"></i></h4>
		<div class="hide">
			<input type="text">
			<div class="radius">
				<label>Search Radius, km</label>
				<input class="radius-num" type="text" value="10"><span class="icon-plus inc"></span><span
						class="icon-minus dec"></span>
			</div>
		</div>
	</div>
	<hr class="sline">
	<div class="price drop open">
		<h4 class="title icon-biweekly">Biweekly Payment<i class="icon-plus"></i></h4>
		<div class="hide">
			<div class="range-wrapper">
				<div class="range-slider">
					<input class="js-range-slider" type="text" value="" data-min="0" data-postfix="$"
						   data-max="100000" data-step="5000">
				</div>
				<div class="extra-controls form-inline">
					<div class="form-group">
						<input class="js-input-from form-control" type="text" value="0">
						<input class="js-input-to form-control" type="text" value="0">
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr class="sline">
	<div class="price drop open">
		<h4 class="title icon-price">Price Range<i class="icon-plus"></i></h4>
		<div class="hide">
			<div class="range-wrapper">
				<div class="range-slider">
					<input class="js-range-slider" type="text" value="" data-min="0" data-postfix="$"
						   data-max="100000" data-step="5000">
				</div>
				<div class="extra-controls form-inline">
					<div class="form-group">
						<input class="js-input-from form-control" type="text" value="0">
						<input class="js-input-to form-control" type="text" value="0">
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr class="sline">
	<div class="make drop">
		<h4 class="title icon-make">Make<i class="icon-plus"></i></h4>
		<div class="hide">
			<div class="checkbox">
				<input id="acura" type="checkbox">
				<label class="icon-check" for="acura">Acura</label>
			</div>
			<div class="checkbox">
				<input id="audi" type="checkbox">
				<label class="icon-check" for="audi">Audi</label>
			</div>
			<div class="checkbox">
				<input id="bmw" type="checkbox">
				<label class="icon-check" for="bmw">BMW</label>
			</div>
			<div class="checkbox">
				<input id="fiat" type="checkbox">
				<label class="icon-check" for="fiat">Fiat</label>
			</div>
			<div class="checkbox">
				<input id="ford" type="checkbox">
				<label class="icon-check" for="ford">Ford</label>
			</div>
			<div class="checkbox">
				<input id="honda" type="checkbox">
				<label class="icon-check" for="honda">Honda</label>
			</div>
			<div class="checkbox">
				<input id="hyundai" type="checkbox">
				<label class="icon-check" for="hyundai">Hyundai</label>
			</div>
			<div class="checkbox">
				<input id="jaguar" type="checkbox">
				<label class="icon-check" for="jaguar">Jaguar</label>
			</div>
			<div class="checkbox">
				<input id="land-rover" type="checkbox">
				<label class="icon-check" for="land-rover">Land Rover</label>
			</div>
			<div class="checkbox">
				<input id="maserati" type="checkbox">
				<label class="icon-check" for="maserati">Maserati</label>
			</div>
			<div class="checkbox">
				<input id="mazda" type="checkbox">
				<label class="icon-check" for="mazda">Mazda</label>
			</div>
			<div class="checkbox">
				<input id="mercedes-menz" type="checkbox">
				<label class="icon-check" for="mercedes-menz">Mercedes-Benz</label>
			</div>
			<div class="checkbox">
				<input id="mitsubishi" type="checkbox">
				<label class="icon-check" for="mitsubishi">Mitsubishi</label>
			</div>
			<div class="checkbox">
				<input id="porsche" type="checkbox">
				<label class="icon-check" for="porsche">Porsche</label>
			</div>
			<div class="checkbox">
				<input id="tesla" type="checkbox">
				<label class="icon-check" for="tesla">Tesla</label>
			</div>
			<div class="checkbox">
				<input id="toyota" type="checkbox">
				<label class="icon-check" for="toyota">Toyota</label>
			</div>
			<div class="checkbox">
				<input id="volkswagen" type="checkbox">
				<label class="icon-check" for="volkswagen">Volkswagen</label>
			</div>
			<div class="checkbox">
				<input id="volvo" type="checkbox">
				<label class="icon-check" for="volvo">Volvo</label>
			</div>
		</div>
	</div>
	<hr class="sline">
	<div class="model drop disable">
		<h4 class="title icon-model">Model<i class="icon-plus"></i></h4>
		<div class="hide">
			<div class="checkbox">
				<input id="2-series" type="checkbox">
				<label class="icon-check" for="2-series">2 Series</label>
			</div>
			<div class="checkbox">
				<input id="3-series" type="checkbox">
				<label class="icon-check" for="3-series">3 Series</label>
			</div>
			<div class="checkbox">
				<input id="4-series" type="checkbox">
				<label class="icon-check" for="4-series">4 Series</label>
			</div>
			<div class="checkbox">
				<input id="4-series-gran-coupe" type="checkbox">
				<label class="icon-check" for="4-series-gran-coupe">4-Series Gran Coupe</label>
			</div>
			<div class="checkbox">
				<input id="5-series" type="checkbox">
				<label class="icon-check" for="5-series">5-Series</label>
			</div>
			<div class="checkbox">
				<input id="fiat-500" type="checkbox">
				<label class="icon-check" for="fiat-500">500</label>
			</div>
			<div class="checkbox">
				<input id="a3" type="checkbox">
				<label class="icon-check" for="a3">A3</label>
			</div>
			<div class="checkbox">
				<input id="a4" type="checkbox">
				<label class="icon-check" for="a4">A4</label>
			</div>
			<div class="checkbox">
				<input id="a5" type="checkbox">
				<label class="icon-check" for="a5">A5</label>
			</div>
			<div class="checkbox">
				<input id="a7" type="checkbox">
				<label class="icon-check" for="a7">A7</label>
			</div>
			<div class="checkbox">
				<input id="accord" type="checkbox">
				<label class="icon-check" for="accord">Accord</label>
			</div>
			<div class="checkbox">
				<input id="c-class" type="checkbox">
				<label class="icon-check" for="c-class">C-Class</label>
			</div>
			<div class="checkbox">
				<input id="cayenne" type="checkbox">
				<label class="icon-check" for="cayenne">Cayenne</label>
			</div>
			<div class="checkbox">
				<input id="corolla" type="checkbox">
				<label class="icon-check" for="corolla">Corolla</label>
			</div>
			<div class="checkbox">
				<input id="f-pace" type="checkbox">
				<label class="icon-check" for="f-pace">F-Pace</label>
			</div>
			<div class="checkbox">
				<input id="ghibli" type="checkbox">
				<label class="icon-check" for="ghibli">Ghibli</label>
			</div>
			<div class="checkbox">
				<input id="gti" type="checkbox">
				<label class="icon-check" for="gti">GTI</label>
			</div>
			<div class="checkbox">
				<input id="lancer" type="checkbox">
				<label class="icon-check" for="lancer">Lancer</label>
			</div>
			<div class="checkbox">
				<input id="macan" type="checkbox">
				<label class="icon-check" for="macan">Macan</label>
			</div>
			<div class="checkbox">
				<input id="mazda3" type="checkbox">
				<label class="icon-check" for="mazda3">MAZDA3</label>
			</div>
			<div class="checkbox">
				<input id="model-s" type="checkbox">
				<label class="icon-check" for="model-s">Model S</label>
			</div>
			<div class="checkbox">
				<input id="mustang" type="checkbox">
				<label class="icon-check" for="mustang">Mustang</label>
			</div>
			<div class="checkbox">
				<input id="q5" type="checkbox">
				<label class="icon-check" for="q5">Q5</label>
			</div>
			<div class="checkbox">
				<input id="range-rover" type="checkbox">
				<label class="icon-check" for="range-rover">Range Rover</label>
			</div>
			<div class="checkbox">
				<input id="range-rover-evoque" type="checkbox">
				<label class="icon-check" for="range-rover-evoque">Range Rover Evoque</label>
			</div>
			<div class="checkbox">
				<input id="range-rover-sport" type="checkbox">
				<label class="icon-check" for="range-rover-sport">Range Rover Sport</label>
			</div>
			<div class="checkbox">
				<input id="s5" type="checkbox">
				<label class="icon-check" for="s5">S5</label>
			</div>
			<div class="checkbox">
				<input id="tiguan" type="checkbox">
				<label class="icon-check" for="tiguan">Tiguan</label>
			</div>
			<div class="checkbox">
				<input id="tlx" type="checkbox">
				<label class="icon-check" for="tlx">TLX</label>
			</div>
			<div class="checkbox">
				<input id="tucson" type="checkbox">
				<label class="icon-check" for="tucson">Tucson</label>
			</div>
			<div class="checkbox">
				<input id="x3" type="checkbox">
				<label class="icon-check" for="x3">X3</label>
			</div>
			<div class="checkbox">
				<input id="x4" type="checkbox">
				<label class="icon-check" for="x4">X4</label>
			</div>
			<div class="checkbox">
				<input id="x5" type="checkbox">
				<label class="icon-check" for="x5">X5</label>
			</div>
			<div class="checkbox">
				<input id="xc90" type="checkbox">
				<label class="icon-check" for="xc90">XC90</label>
			</div>
		</div>
	</div>
	<hr class="sline">
	<div class="year drop">
		<h4 class="title icon-calendar">Year<i class="icon-plus"></i></h4>
		<div class="hide">
			<div class="checkbox">
				<input id="year-2014" type="checkbox">
				<label class="icon-check" for="year-2014">2014</label>
			</div>
			<div class="checkbox">
				<input id="year-2015" type="checkbox">
				<label class="icon-check" for="year-2015">2015</label>
			</div>
			<div class="checkbox">
				<input id="year-2016" type="checkbox">
				<label class="icon-check" for="year-2016">2016</label>
			</div>
			<div class="checkbox">
				<input id="year-2017" type="checkbox">
				<label class="icon-check" for="year-2017">2017</label>
			</div>
			<div class="checkbox">
				<input id="year-2018" type="checkbox">
				<label class="icon-check" for="year-2018">2018</label>
			</div>
			<div class="checkbox">
				<input id="year-2019" type="checkbox">
				<label class="icon-check" for="year-2019">2019</label>
			</div>
			<div class="checkbox">
				<input id="year-2020" type="checkbox">
				<label class="icon-check" for="year-2020">2020</label>
			</div>
		</div>
	</div>
	<hr class="sline">
	<div class="body-type drop">
		<h4 class="title icon-body-type">Body Type<i class="icon-plus"></i></h4>
		<div class="hide">
			<div class="checkbox">
				<input id="truck" type="checkbox">
				<label class="icon-check" for="truck">Truck</label>
			</div>
			<div class="checkbox">
				<input id="suv" type="checkbox">
				<label class="icon-check" for="suv">Suv</label>
			</div>
			<div class="checkbox">
				<input id="van" type="checkbox">
				<label class="icon-check" for="van">Van</label>
			</div>
			<div class="checkbox">
				<input id="sedan" type="checkbox">
				<label class="icon-check" for="sedan">Sedan</label>
			</div>
			<div class="checkbox">
				<input id="coupe" type="checkbox">
				<label class="icon-check" for="coupe">Coupe</label>
			</div>
		</div>
	</div>
	<hr class="sline">
	<div class="fuel drop">
		<h4 class="title icon-fuel">Fuel type<i class="icon-plus"></i></h4>
		<div class="hide">
			<div class="checkbox">
				<input id="diesel" type="checkbox">
				<label class="icon-check" for="diesel">Diesel</label>
			</div>
			<div class="checkbox">
				<input id="electric" type="checkbox">
				<label class="icon-check" for="electric">Electric</label>
			</div>
			<div class="checkbox">
				<input id="gas" type="checkbox">
				<label class="icon-check" for="gas">Gas</label>
			</div>
		</div>
	</div>
	<hr class="sline">
	<div class="drivetrain drop">
		<h4 class="title icon-drivetrain">Drivetrain<i class="icon-plus"></i></h4>
		<div class="hide">
			<div class="checkbox">
				<input id="awd" type="checkbox">
				<label class="icon-check" for="awd">AWD</label>
			</div>
			<div class="checkbox">
				<input id="fwd" type="checkbox">
				<label class="icon-check" for="fwd">FWD</label>
			</div>
			<div class="checkbox">
				<input id="rwd" type="checkbox">
				<label class="icon-check" for="rwd">RWD</label>
			</div>
		</div>
	</div>
	<hr class="sline">
	<div class="exterior drop">
		<h4 class="title icon-exterior">Exterior Color<i class="icon-plus"></i></h4>
		<div class="hide">
			<div class="checkbox">
				<input id="ex-black" type="checkbox">
				<label class="icon-check" for="ex-black">Black</label>
			</div>
			<div class="checkbox">
				<input id="ex-blue" type="checkbox">
				<label class="icon-check" for="ex-blue">Blue</label>
			</div>
			<div class="checkbox">
				<input id="ex-brown" type="checkbox">
				<label class="icon-check" for="ex-brown">Brown</label>
			</div>
			<div class="checkbox">
				<input id="ex-dark-blue" type="checkbox">
				<label class="icon-check" for="ex-dark-blue">Dark Blue</label>
			</div>
			<div class="checkbox">
				<input id="ex-dark-grey" type="checkbox">
				<label class="icon-check" for="ex-dark-grey">Dark Grey</label>
			</div>
			<div class="checkbox">
				<input id="ex-gray" type="checkbox">
				<label class="icon-check" for="ex-gray">Gray</label>
			</div>
			<div class="checkbox">
				<input id="ex-red" type="checkbox">
				<label class="icon-check" for="ex-red">Red</label>
			</div>
			<div class="checkbox">
				<input id="ex-silver" type="checkbox">
				<label class="icon-check" for="ex-silver">Silver</label>
			</div>
			<div class="checkbox">
				<input id="ex-white" type="checkbox">
				<label class="icon-check" for="ex-white">White</label>
			</div>
		</div>
	</div>
	<hr class="sline">
	<div class="transmission drop">
		<h4 class="title icon-transmission">Transmission<i class="icon-plus"></i></h4>
		<div class="hide">
			<div class="checkbox">
				<input id="automatic" type="checkbox">
				<label class="icon-check" for="automatic">Automatic</label>
			</div>
			<div class="checkbox">
				<input id="manual" type="checkbox">
				<label class="icon-check" for="manual">Manual</label>
			</div>
		</div>
	</div>
	<hr class="sline">
	<div class="engine drop">
		<h4 class="title icon-engine">Engine<i class="icon-plus"></i></h4>
		<div class="hide">
			<div class="checkbox">
				<input id="l-1-4" type="checkbox">
				<label class="icon-check" for="l-1-4">1.4 L</label>
			</div>
			<div class="checkbox">
				<input id="l-1-8" type="checkbox">
				<label class="icon-check" for="l-1-8">1.8 L</label>
			</div>
			<div class="checkbox">
				<input id="l-2" type="checkbox">
				<label class="icon-check" for="l-2">2 L</label>
			</div>
			<div class="checkbox">
				<input id="l-2-5" type="checkbox">
				<label class="icon-check" for="l-2-5">2.5 L</label>
			</div>
			<div class="checkbox">
				<input id="l-3" type="checkbox">
				<label class="icon-check" for="l-3">3 L</label>
			</div>
			<div class="checkbox">
				<input id="l-3-5" type="checkbox">
				<label class="icon-check" for="l-3-5">3.5 L</label>
			</div>
			<div class="checkbox">
				<input id="l-3-6" type="checkbox">
				<label class="icon-check" for="l-3-6">3.6 L</label>
			</div>
			<div class="checkbox">
				<input id="l-5" type="checkbox">
				<label class="icon-check" for="l-5">5 L</label>
			</div>
		</div>
	</div>
	<hr class="sline">
	<div class="interior drop">
		<h4 class="title icon-interior">Interior Color<i class="icon-plus"></i></h4>
		<div class="hide">
			<div class="checkbox">
				<input id="in-black" type="checkbox">
				<label class="icon-check" for="in-black">Black</label>
			</div>
			<div class="checkbox">
				<input id="in-brown" type="checkbox">
				<label class="icon-check" for="in-brown">Brown</label>
			</div>
			<div class="checkbox">
				<input id="in-cream" type="checkbox">
				<label class="icon-check" for="in-cream">Cream</label>
			</div>
			<div class="checkbox">
				<input id="in-gray" type="checkbox">
				<label class="icon-check" for="in-gray">Gray</label>
			</div>
			<div class="checkbox">
				<input id="in-grey" type="checkbox">
				<label class="icon-check" for="in-grey">Grey</label>
			</div>
			<div class="checkbox">
				<input id="in-red" type="checkbox">
				<label class="icon-check" for="in-red">Red</label>
			</div>
			<div class="checkbox">
				<input id="in-tan" type="checkbox">
				<label class="icon-check" for="in-tan">Tan</label>
			</div>
		</div>
	</div>
	<hr class="sline">
	<button class="clear-all button" type="rest">Reset all</button>
</form>
