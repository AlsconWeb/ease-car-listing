<?php
/**
 * Sort form template.
 *
 * @package iwpdev/ease-car-listing
 */

?>

<p class="matches">213 matches</p>
<div class="sort dfr">
	<p>Sort by:</p>
	<select id="sort-item">
		<option value="date_high">Date: newest first</option>
		<option value="date_low">Date: oldest first</option>
		<option value="price_low">Price: lower first</option>
		<option value="price_high">Price: highest first</option>
		<option value="mileage_low">Mileage: lowest first</option>
		<option value="mileage_high">Mileage: highest first</option>
	</select>
</div>
