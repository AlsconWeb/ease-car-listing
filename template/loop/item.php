<?php
/**
 * Car item template.
 *
 * @package iwpdev/ease-car-listing
 */
?>
<div class="car-item">
	<div class="img">
		<img src="img/chery-a-13-1.jpg" alt="">
	</div>
	<div class="car-meta-top heading-font">
		<div class="title-wrapper m-price">
			<h4 class="car-title">2011 | Chery | A-13 | Luxury</h4>
			<div class="price">
				<p class="monthly-price">$275.23<span>biweekly</span></p>
				<div class="price discounted-price"></div>
			</div>
		</div>
		<div class="carnance-price-wrapper">
			<p class="mileage">50 614 km</p>
			<p class="normal-price">$27,999.00</p>
		</div>
	</div>
	<a class="link" href="#"></a>
</div>
