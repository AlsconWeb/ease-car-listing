<?php
/**
 * Template single car.
 *
 * @package iwpdev/ease-car-listing
 */

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();

		$mileage           = carbon_get_the_post_meta( 'ecl_car_mileage' );
		$gallery_image_ids = carbon_get_the_post_meta( 'ecl_gallery' );
		$fmt               = numfmt_create( 'en_US', NumberFormatter::CURRENCY );
		$price             = carbon_get_the_post_meta( 'ecl_price' );
		$by_month_price    = carbon_get_the_post_meta( 'ecl_by_month' );
		$sales_price       = carbon_get_the_post_meta( 'ecl_sales_price' );
		?>
		<div class="container">
			<h1 class="title"><?php the_title(); ?></h1>
			<?php
			echo sprintf(
				'<p class="mileage">%d km</p>',
				esc_attr( number_format( $mileage, 0, '.', ',' ) ?? 0 )
			);
			?>
			<div class="dfr">
				<?php if ( ! empty( $gallery_image_ids ) && count( $gallery_image_ids ) > 1 ) { ?>
					<div class="gallery">
						<div class="car-gallery">
							<?php foreach ( $gallery_image_ids as $gallery_image_id ) { ?>
								<div class="item">
									<a
											href="<?php echo esc_url( get_the_permalink( $gallery_image_id ) ); ?>"
											data-fancybox="gallery">
										<img
												src="<?php echo esc_url( wp_get_attachment_image_url( $gallery_image_id, 'full' ) ); ?>"
												alt="<?php echo esc_html( get_the_title( $gallery_image_id ) ); ?>">
									</a>
								</div>
							<?php } ?>
						</div>
						<div class="car-gallery-nav">
							<?php foreach ( $gallery_image_ids as $gallery_image_id ) { ?>
								<div class="item">
									<img
											src="<?php echo esc_url( wp_get_attachment_image_url( $gallery_image_id, 'thumbnail' ) ); ?>"
											alt="<?php echo esc_html( get_the_title( $gallery_image_id ) ); ?>">
								</div>
							<?php } ?>
						</div>
					</div>
				<?php } ?>
				<div class="description">
					<div class="financing">
						<h2 class="title"><?php esc_html_e( 'Estimated Financing', 'ease-car-listing' ); ?></h2>
						<?php if ( ! empty( $price ) ) { ?>
							<div class="dfr">
								<h3 class="title">
									<?php esc_html_e( 'Price', 'ease-car-listing' ); ?>
									<i class="icon-info"></i>
									<p class="tooltip">
										<?php esc_html_e( 'List price does not include taxes or registration.', 'ease-car-listing' ); ?>
									</p>
								</h3>
								<p class="price">
									<?php echo numfmt_format_currency( $fmt, $price, "USD" ); ?>
								</p>
								<?php if ( ! empty( $sales_price ) ) { ?>
									<p class="sales-price">
										<?php echo numfmt_format_currency( $fmt, $sales_price, "USD" ); ?>
									</p>
								<?php } ?>
							</div>
						<?php } ?>
						<?php if ( ! empty( $by_month_price ) ) { ?>
							<div class="dfr">
								<h3 class="title">
									<?php esc_html_e( 'Price on credit per month', 'ease-car-listing' ); ?>
									<i class="icon-info"></i>
									<p class="tooltip">
										<?php esc_html_e( 'List price does not include taxes or registration.', 'ease-car-listing' ); ?>
									</p>
								</h3>
								<?php
								echo sprintf(
									'<p class="beweekly-price">%s %s</p>',
									numfmt_format_currency( $fmt, $by_month_price, "USD" ),
									esc_html( __( 'per month', 'ease-car-listing' ) )
								);
								?>
							</div>
						<?php } ?>
						<a class="button" href="#">
							<?php esc_html_e( 'Apply Now', 'ease-car-listing' ); ?>
						</a>
					</div>
				</div>
				<?php
				$car_year         = carbon_get_the_post_meta( 'ecl_car_year' );
				$car_drivetrain   = carbon_get_the_post_meta( 'ecl_drive_train_type' );
				$car_exterior     = carbon_get_the_post_meta( 'ecl_body_color' );
				$car_transmission = carbon_get_the_post_meta( 'ecl_transmission_type' );
				$car_engine       = wp_get_post_terms( get_the_ID(), 'engine' )[0];
				$car_interior     = carbon_get_the_post_meta( 'ecl_interior_color' );
				?>
				<ul class="dfr car-description">
					<?php if ( ! empty( $car_year ) ) { ?>
						<li>
							<i class="icon-calendar"></i>
							<h5 class="title">
								<span><?php esc_html_e( 'Year', 'ease-car-listing' ); ?></span>
								<?php echo esc_html( $car_year ); ?>
							</h5>
						</li>
					<?php } ?>
					<?php if ( ! empty( $car_mileage ) ) { ?>
						<li>
							<i class="icon-mileage"></i>
							<h5 class="title">
								<span><?php esc_html_e( 'Mileage', 'ease-car-listing' ); ?></span>
								<?php
								echo sprintf(
									'%d KM',
									esc_attr( number_format( $mileage, 0, '.', ',' ) ?? 0 )
								);
								?>
							</h5>
						</li>
					<?php } ?>
					<?php if ( ! empty( $car_drivetrain ) ) { ?>
						<li>
							<i class="icon-drivetrain"></i>
							<h5 class="title">
								<span><?php esc_html_e( 'DriveTrain', 'ease-car-listing' ); ?></span>
								<?php echo esc_html( strtoupper( $car_drivetrain ) ); ?>
							</h5>
						</li>
					<?php } ?>
					<?php if ( ! empty( $car_exterior ) ) { ?>
						<li>
							<i class="icon-exterior"></i>
							<h5 class="title">
								<span><?php esc_html_e( 'Exterior Color', 'ease-car-listing' ); ?></span>
								<?php echo esc_html( strtoupper( $car_exterior ) ); ?>
							</h5>
						</li>
					<?php } ?>
					<?php if ( ! empty( $car_transmission ) ) { ?>
						<li>
							<i class="icon-transmission"></i>
							<h5 class="title">
								<span><?php esc_html_e( 'Transmission', 'ease-car-listing' ); ?></span>
								<?php echo esc_html( $car_transmission ); ?>
							</h5>
						</li>
					<?php } ?>
					<?php if ( ! empty( $car_engine ) ) { ?>
						<li>
							<i class="icon-engine"></i>
							<h5 class="title">
								<span><?php esc_html_e( 'Engine', 'ease-car-listing' ); ?></span>
								<?php echo esc_html( $car_engine->name ); ?>
							</h5>
						</li>
					<?php } ?>
					<?php if ( ! empty( $car_interior ) ) { ?>
						<li>
							<i class="icon-interior"></i>
							<h5 class="title">
								<span><?php esc_html_e( 'Interior Color', 'ease-car-listing' ); ?></span>
								<?php echo esc_html( $car_interior ); ?>
							</h5>
						</li>
					<?php } ?>
				</ul>
			</div>
			<?php
			$car_features = explode( ', ', carbon_get_the_post_meta( 'ecl_features' ) );
			if ( ! empty( $car_features ) ) {
				?>
				<div id="features">
					<h2 class="title"><?php esc_html_e( 'Features', 'ease-car-listing' ); ?></h2>
					<ul class="features-items">
						<?php foreach ( $car_features as $car_feature ) { ?>
							<li class="item icon-check">
								<?php echo esc_html( $car_feature ); ?>
							</li>
						<?php } ?>
					</ul>
					<a class="button" id="featured-more" href="#">
						<?php esc_html_e( 'Show Less', 'ease-car-listing' ); ?>
					</a>
				</div>
			<?php } ?>
			<?php
			$car_additional = carbon_get_the_post_meta( 'ecl_additional_info' );

			if ( ! empty( $car_additional ) ) {
				?>
				<div class="information">
					<h2><?php esc_html_e( 'Additional Information', 'ease-car-listing' ); ?></h2>
					<?php echo wp_kses_post( wpautop( $car_additional ) ); ?>
				</div>
			<?php } ?>
		</div>
		<?php

		the_content();
	}
}
get_footer();
